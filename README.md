# Security Metrics Catalogue Description #
The Security Metrics Catalogue is a component useful to store and manage all the information about the Security Metrics. It enables the CRUD operations on each of them.
The main design requirement is that should be possible to get all the Security Metrics starting from their unique identifier (Reference ID). Moreover, the component allows the end user to get an xml file that represents a specific Security Metric. Finally, the end user has the possibility to extract an SQLite file (.db extention) that contains all the Security Metrics stored in the component

### Use Cases ###

![Metric Catalog 4 - Use Case.jpg](https://bitbucket.org/repo/agXArj/images/2756134005-Metric%20Catalog%204%20-%20Use%20Case.jpg)

## Installation ##

**Install using precompiled binaries**

The precompiled binaries are available under the SPECS Artifact Repository (http://ftp.specs-project.eu/public/artifacts/)

Prerequisites:

* Oracle Java JDK 7;
* SQLite 3.9.x;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);

Installation steps:

* download the web application archive (war) file from the artifact repository :
http://ftp.specs-project.eu/public/artifacts/sla-platform/metric-catalogue/metric-catalogue-STABLE.war
* the war file has to be deployed in the java servlet/web container

If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.

**Compile and install from source**

In order to compile and install the Security Metrics Catalogue it is mandatory first to process the backend and afterwards the frontend.

Prerequisites:

* a Git client;
* Apache Maven 3.3.x;
* Oracle Java JDK 7;
* SQLite 3.9.x;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);

Backend installation steps:

* clone the Bitbucket repository:
```
#!bash
git clone git@bitbucket.org:specs-team/specs-core-sla_platform-security-metric-catalogue.git
```
* change the configuration of the database in the persistence.xml file (the file is located in the folder “src/main/resource” and “/src/test/resources”) in order to define the correct path where the SQLite database will be created;
* under specs-core-sla_platform-security-metric-catalogue run:
```
#!bash
mvn install
```

Frontend installation steps:

* clone the Bitbucket repository:
```
#!bash
git clone git@bitbucket.org:specs-team/specs-core-sla_platform-security-metric-catalogue-api.git
```
* under specs-core-sla_platform-security-metric-catalogue-api run:
```
#!bash
mvn package
```

The backend installation generates the artifact used by the frontend. The frontend installation generates a web application archive (war) file, under the “/target” subfolder. In order to use the component, the war file has to be deployed in the java servlet/web container. If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.


## Usage ##
The Security Metrics Catalogue exposes a REST API interface. All the exposed REST resources are mapped under the path “/cloud-sla/*”. The mapping rules are defined in the web.xml file under WEB-INF folder (CATALINA_HOME/webapps/metric-catalogue-api/WEB-INF/):

```
#!xml
<servlet-mapping>
		<servlet-name>Jersey REST Service</servlet-name>
		<url-pattern>/cloud-sla/*</url-pattern>
</servlet-mapping>
```

Moreover, all the REST resources are represented by specific java classes under the package “eu.specsproject.slaplatform.metriccatalogue.restfrontend”, defined in the web.xml file under WEB-INF folder:

```
#!xml
<init-param>
	<param-name>jersey.config.server.provider.packages</param-name>
	<param-value>eu.specsproject.slaplatform.metriccatalogue.restfrontend</param-value>
</init-param>
```

At the startup of the component, all the REST resources are configured based on the configuration parameters defined in the web.xml file.


##Rest API Calls - Example##

![API 1.png](https://bitbucket.org/repo/agXArj/images/3799429594-API%201.png)

![API 2.png](https://bitbucket.org/repo/agXArj/images/4097258055-API%202.png)

### Who do I talk to? ###

* Please contact massimiliano.rak@unina2.it
* www.specs-project.eu

SPECS Project 2013 - CeRICT