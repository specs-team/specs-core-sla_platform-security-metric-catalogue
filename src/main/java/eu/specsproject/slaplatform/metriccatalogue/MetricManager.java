/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetric;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricDocument;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;


/**
 *  The Service_Manager permits to manage Security Mechanisms and the Capabilites and Metrics
 *  associated.
 * 
 * @author Pasquale De Rosa SPECS  CeRICT
 * 
 */
public interface MetricManager {

    //************SECURITY MECHANISMS FUNCTIONS*************//

    /**
     * Create and persist a Security Capability into the SLA Platform.
     * The Security Capability has to be not null, well formed and valid (see {@link SecurityMetricDocument}), otherwise an {@link IllegalArgumentException} will be thrown.
     * 
     * @param securityCapability  A valid and well formed Security Capability to persist.
     * @return an opaque object that identifies the created Security Capability, intended to be used to perform further operation on it.
     * @throws Exception 
     * @throws IllegalArgumentException in case of SLA argument is null, not valid nor well formed.
     */
    public SecurityMetricIdentifier createSMT(SecurityMetricDocument securityMetricDocument) throws Exception;


    /**
     * Retrieve a Security Mechanism from the persistence.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SecurityMechanism identifier.
     * @return the persisted Security Mechanism
     * @throws IllegalArgumentException in case of Security Mechanism argument is null, not valid nor well formed.
     */
    public SecurityMetric retrieveSMT(SecurityMetricIdentifier id);

    public SecurityMetricIdentifier removeSMT(SecurityMetricIdentifier id);
    
    /**
     * Retrieve all Security Mechanisms from the persistence.
     * 
     * @return all the persisted Security Mechanism
     * @throws IllegalArgumentException in case of Security Mechanism argument is null, not valid nor well formed.
     */
    public List<SecurityMetricIdentifier> searchSMTs(String metricType);
    
    public File getMetricsBackup(String dbName);
    
    public Boolean restoreMetricsBackup(InputStream dbFile, String dbName);
    
    /**
     * Edits a Security Mechanisms in the persistence.
     * An {@link IllegalArgumentException} will be thrown in case of a null or not valid identifier is used as argument.
     * 
     * @param id  a valid and not null SecurityMechanism identifier.
     * @param securityMechanism  a valid and not null SecurityMechanism_Document.
     * @throws IllegalArgumentException in case of SecurityMechanism_Document argument is null, not valid nor well formed.
     */
    public SecurityMetricIdentifier updateSMT(SecurityMetricIdentifier id,
            SecurityMetricDocument securityMechanism);

    public void annotateSMT(SecurityMetricIdentifier id, Object object);


    public void getAnnotationsSMT(SecurityMetricIdentifier id);


    public void updateAnnotationSMT(SecurityMetricIdentifier id, String annId,
            Object object);

}
