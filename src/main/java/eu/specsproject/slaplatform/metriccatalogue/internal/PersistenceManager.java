/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue.internal;

import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import eu.specsproject.slaplatform.metriccatalogue.MetricManagerFactory;

public class PersistenceManager {
    
    private PersistenceManager(){
        //Zero arguments constructor
    }
    
//    public static String getSqlPathByUnit(String unitName){
//        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(MetricManagerFactory.DEFAULTUNIT);
//        Map<String, Object> propertiesMap = entityManagerFactory.getProperties();
//        System.out.println("unitName: "+unitName);
//        System.out.println("propertiesMap size: "+propertiesMap.size());
//        System.out.println("javax.persistence.jdbc.url: "+entityManagerFactory.getProperties().get("javax.persistence.jdbc.url").toString());
//
//        
//        for (Map.Entry<String, Object> e : propertiesMap.entrySet()) {
//            System.out.println("Unit key found: "+e.getKey());
//            if("javax.persistence.jdbc.url".equals(e.getKey())){
//                if(unitName.equals(MetricManagerFactory.DEFAULTUNIT)){
//                    return e.getValue().toString();
//                }else{
//                    String sqlDatabasePath = e.getValue().toString();
//                    String dbNome = sqlDatabasePath.split("/")[sqlDatabasePath.split("/").length-1];
//                    return sqlDatabasePath.replace(dbNome, unitName+".db");
//                }
//            }
//        }
//        return null;
//    }
//    
//    public static String getSqlPathByDbName(String dbName){
//        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(MetricManagerFactory.DEFAULTUNIT);
//        
//        Map<String, Object> propertiesMap = entityManagerFactory.getProperties();
//
//        for (Map.Entry<String, Object> e : propertiesMap.entrySet()) {
//            if("javax.persistence.jdbc.url".equals(e.getKey())){
//                    String sqlDatabasePath = e.getValue().toString();
//                    String dbNome = sqlDatabasePath.split("/")[sqlDatabasePath.split("/").length-1];
//                    return sqlDatabasePath.replace(dbNome, dbName+".db");
//            }
//        }
//        return null;
//    }
}
