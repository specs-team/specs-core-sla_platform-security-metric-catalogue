/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue.internal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetric;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;

/**
 * JPA SQL implementation.
 * This code is not intended to be part of the public API.
 * 
 * @author Mauro Turtur SPECS - CeRICT
 *
 */
public final class EUMetricManagerSQLJPA extends EUMetricManagerAbstractImpl {

    private EntityManagerFactory emFactory;

    public EUMetricManagerSQLJPA (EntityManagerFactory factory){
        emFactory = factory;
    }


    //SECURITY MECHANISMS PERSISTENCE MANAGE
    @Override
    void persistenceCreateSMT(SecurityMetric securityMetric) throws Exception{
        System.out.println("CREATE CALL");
        EntityManager em =  emFactory.createEntityManager();
        try{
        EntityTransaction t = em.getTransaction();
        System.out.println("begin");
        t.begin();
        System.out.println("persist");
        em.persist(securityMetric);
        System.out.println("commit");
        t.commit();
        System.out.println("close");
        em.close();
        }catch(Exception e){
            System.out.println("close");
            em.close();
            System.out.println("EXCEPTION");
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    SecurityMetric persistenceGetSMTByID(SecurityMetricIdentifier id) {
        if (id==null)
            throw new IllegalArgumentException("SecurityMetric_Identifier cannot be null");
        String iD = id.getId();
        SecurityMetric securityMetric = emFactory.createEntityManager().find(SecurityMetric.class,iD);

        if (securityMetric==null)
            throw new IllegalArgumentException("SecurityMetric_Identifier is not valid");

        return securityMetric;
    }
    
    @Override
    List<SecurityMetricIdentifier> persistenceSearchSMT(String metricType){

        EntityManager em =  emFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SecurityMetric> cq = cb.createQuery(SecurityMetric.class);
        Root<SecurityMetric> rootEntry = cq.from(SecurityMetric.class);
        CriteriaQuery<SecurityMetric> all = cq.select(rootEntry);
        TypedQuery<SecurityMetric> allQuery = em.createQuery(all);

        List<SecurityMetricIdentifier> result = new ArrayList<SecurityMetricIdentifier>();

        if(metricType == null || "".equals(metricType)){
            for (SecurityMetric securityMetric :  allQuery.getResultList()){
                result.add(new SecurityMetricIdentifier(securityMetric.referenceId));
            }
        }else{
            for (SecurityMetric securityMetric :  allQuery.getResultList()){
                if(securityMetric.metricType.equals(metricType)){
                    result.add(new SecurityMetricIdentifier(securityMetric.referenceId));
                }
            }
        }

        em.close();
        return result;
    }

    @Override
    void persistenceUpdateSMT(SecurityMetric securityMetric) {
        EntityManager em =  emFactory.createEntityManager();
        EntityTransaction t = em.getTransaction();
        t.begin();
        securityMetric.updated=new Date();
        em.merge(securityMetric);
        t.commit();
        em.close();
    }

    @Override
    void persistenceRemoveSMTByID(SecurityMetricIdentifier id) {
        if (id==null)
            throw new IllegalArgumentException("SecurityMetric_Identifier cannot be null");
        String iD = id.getId();
        EntityManager em = emFactory.createEntityManager();
        SecurityMetric securityMetric = em.find(SecurityMetric.class,iD);

        if (securityMetric==null)
            throw new IllegalArgumentException("SecurityMetric_Identifier is not valid");

        em.getTransaction().begin();
        em.remove(securityMetric);
        em.getTransaction().commit();

    }
}
