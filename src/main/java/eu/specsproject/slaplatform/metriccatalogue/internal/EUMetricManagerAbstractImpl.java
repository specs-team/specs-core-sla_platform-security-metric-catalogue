/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue.internal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import eu.specsproject.slaplatform.metricatalogue.utilities.PropertiesManager;
import eu.specsproject.slaplatform.metriccatalogue.MetricManager;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetric;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricDocument;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;

public abstract class EUMetricManagerAbstractImpl implements MetricManager{


    //SECURITY MECHANISMS PERSISTENCE FUNCTIONS
    /**
     * 
     * The method implementation must ensure that the identifier is both not null and valid. 
     * Otherwise, an {@link IllegalArgumentException} has to be thrown. 
     * 
     * @param id - SecurityMechanism identifier, not null 
     * @return the queried SecurityMechanism from persistence
     * @throws IllegalArgumentException in case id is null or SecurityMechanism is not found
     */
    abstract SecurityMetric  persistenceGetSMTByID(SecurityMetricIdentifier id);

    /**
     * Create a SecurityMechanism in persistence.
     * 
     * @param securityMechanism - the SecurityMechanism to persist, not null
     * @throws Exception 
     */
    abstract void persistenceCreateSMT(SecurityMetric securityMetric) throws Exception;

    /**
     * Search all SecurityMechanisms in persistence.
     * 
     */
    abstract List<SecurityMetricIdentifier> persistenceSearchSMT(String metricType);

    /**
     * update a Security Mechanism in persistence.
     * 
     * @param securityMechanism  - the Security Mechanism to update, not null
     */
    abstract void persistenceUpdateSMT(SecurityMetric securityMetric);

    abstract void persistenceRemoveSMTByID(SecurityMetricIdentifier id);

    //SECURITY METRICS FUNCTIONS

    @Override
    public SecurityMetricIdentifier createSMT(SecurityMetricDocument securityMetricDocument) throws Exception{
        if (securityMetricDocument==null)
            throw new IllegalArgumentException("The SecurityMetric_Document cannot be null");

        //create SecurityMetric
        SecurityMetric internalSecurityMetric = new SecurityMetric();

        internalSecurityMetric = new Gson().fromJson(securityMetricDocument.getSecurityMetricJsonDocument(), SecurityMetric.class);

        persistenceCreateSMT(internalSecurityMetric);

        return new  SecurityMetricIdentifier(internalSecurityMetric.referenceId);
    }

    @Override
    public SecurityMetric retrieveSMT(SecurityMetricIdentifier id) {

        return  persistenceGetSMTByID(id);  
    }

    @Override
    public SecurityMetricIdentifier removeSMT(SecurityMetricIdentifier id){
        persistenceRemoveSMTByID(id);
        return id;
    }

    @Override
    public List<SecurityMetricIdentifier> searchSMTs(String metricType){
        return persistenceSearchSMT(metricType);
    }



    @Override
    public SecurityMetricIdentifier updateSMT(SecurityMetricIdentifier id,
            SecurityMetricDocument securityMetricDocument){

        SecurityMetric intsm = persistenceGetSMTByID(id);

        if (securityMetricDocument==null)
            throw new IllegalArgumentException("SecurityMetric_Document cannot be null");

        intsm = new Gson().fromJson(securityMetricDocument.getSecurityMetricJsonDocument(), SecurityMetric.class);

        persistenceUpdateSMT(intsm);
        return id;
    }

    @Override
    public File getMetricsBackup(String dbName){
        String sqlDatabasePath = PropertiesManager.getProperty("metric_catalogue.databasePath")+dbName+".db";
        String filePath = sqlDatabasePath;
        File downloadFile = new File(filePath);
        if(downloadFile.exists()){
            return downloadFile;
        }else{
            return null;
        }
    }

    @Override
    public Boolean restoreMetricsBackup(InputStream dbFile, String dbName){
        String sqlDatabasePath = PropertiesManager.getProperty("metric_catalogue.databasePath")+dbName+".db";
        String filePath = sqlDatabasePath;
        return writeToFile(dbFile, filePath);
    }

    private Boolean writeToFile(InputStream uploadedInputStream,
            String uploadedFileLocation) {

        try {
            OutputStream out = new FileOutputStream(new File(
                    uploadedFileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
            return true;
        } catch (IOException e) {
            LoggerFactory.getLogger(EUMetricManagerAbstractImpl.class).debug("IOException", e);
            return false;
        }

    }


    @Override
    public void annotateSMT(SecurityMetricIdentifier id, Object object) {
    }

    @Override
    public void getAnnotationsSMT(SecurityMetricIdentifier id) {
    }

    @Override
    public void updateAnnotationSMT(SecurityMetricIdentifier id, String annId,
            Object object) {
    }
}
