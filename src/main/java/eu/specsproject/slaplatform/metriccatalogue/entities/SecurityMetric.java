/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue.entities;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class SecurityMetric {

    @Temporal(TemporalType.DATE)
    @Column(nullable = false) 
    Date created;
    
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    public 
    Date updated;
    
    @Id
    @Column(nullable = false)
    public 
    String referenceId;
    
    @Column(nullable = false) 
    String XMLdescription;
    
    @Column(nullable = false)
    public 
    String metricType;
 
    
    public SecurityMetric(){
        created = updated = new Date();
    }

    public void setCreated(Date created){
        this.created = created;
    }
    
    public Date getCreated(){
        return created;
    }
    
    public void setUpdated(Date updated){
        this.updated = updated;
    }
    
    public String getReferenceId(){
        return referenceId;
    }
    
    public void setReferenceId(String referenceId){
        this.referenceId = referenceId;
    }
    
    public Date getUpdated(){
        return updated;
    }
    
    public void setXMLdescription(String XMLdescription){
        this.XMLdescription = XMLdescription;
    }
    
    public String getXMLdescription(){
        return XMLdescription;
    }
    
    public void setMetricType(String metricType){
        this.metricType = metricType;
    }
    
    public String getMetricType(){
        return metricType;
    }

}
