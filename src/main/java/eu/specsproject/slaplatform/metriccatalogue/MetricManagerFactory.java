/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import eu.specsproject.slaplatform.metricatalogue.utilities.PropertiesManager;
import eu.specsproject.slaplatform.metriccatalogue.internal.EUMetricManagerSQLJPA;
import eu.specsproject.slaplatform.metriccatalogue.internal.PersistenceManager;


public final class MetricManagerFactory {
    public static final String DEFAULTUNIT = "default_unit";
    public static final String TEMPLATEUNIT = "template_unit";

    private MetricManagerFactory(){
        //Zero arguments constructor
    }

    public static MetricManager getMetricManagerInstance(){
        return new EUMetricManagerSQLJPA(Persistence.createEntityManagerFactory(DEFAULTUNIT));
    }
    
    public static MetricManager getMetricManagerInstance(String unitName, boolean isTest){
        return new EUMetricManagerSQLJPA(Persistence.createEntityManagerFactory(unitName));
    }


    public static MetricManager getMetricManagerInstance(String dbName){               
                Map<String, String> dbProps = new HashMap<String, String>();            
                dbProps.put("javax.persistence.jdbc.driver", "org.sqlite.JDBC"); 
                dbProps.put("eclipselink.logging.level", "ALL");                     
                dbProps.put("eclipselink.ddl-generation","create-tables");  
                dbProps.put("javax.persistence.jdbc.url", "jdbc:sqlite:"+PropertiesManager.getProperty("metric_catalogue.databasePath")+dbName+".db");
                EntityManagerFactory fact = Persistence.createEntityManagerFactory(TEMPLATEUNIT, dbProps);
                return new EUMetricManagerSQLJPA(fact);
    }
    
}


