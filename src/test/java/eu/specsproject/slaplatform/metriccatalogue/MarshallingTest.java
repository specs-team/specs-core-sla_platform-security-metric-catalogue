package eu.specsproject.slaplatform.metriccatalogue;


import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specsproject.slaplatform.metriccatalogue.internal.marshalling.MarshallingInterface;
import eu.specsproject.slaplatform.metriccatalogue.internal.marshalling.implementation.JSONentityBuilder;
import eu.specsproject.slaplatform.metriccatalogue.internal.marshalling.implementation.JSONmarshaller;
import eu.specsproject.slaplatform.metriccatalogue.internal.marshalling.implementation.XMLentityBuilder;
import eu.specsproject.slaplatform.metriccatalogue.internal.marshalling.implementation.XMLmarshaller;


public class MarshallingTest {

	
	@BeforeClass
	public static void setUpBeforeClass(){
		
	}
	
	@Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
    }
	
	
    @Test
    public void unMarshalTest(){
        JSONentityBuilder builder = new JSONentityBuilder ();
        MarshallingInterface inf = builder.unmarshal("", MarshallingInterface.class);
        Assert.assertNull(inf);
    }
    
    @Test
    public void marshalTest(){
        JSONmarshaller marshaller = new JSONmarshaller();
        MarshallingInterface inf = null;
        String marshString = marshaller.marshal(inf, MarshallingInterface.class);
        Assert.assertNotNull(marshString);
        
    }
    
    @Test
    public void XMLunMarshalTest(){
        XMLentityBuilder builder = new XMLentityBuilder();
        MarshallingInterface inf = builder.unmarshal("XMLmarshallerTest", MarshallingInterface.class);
        Assert.assertNull(inf);
    }
    
    @Test
    public void XMLmarshalTest(){
        XMLmarshaller marshaller = new XMLmarshaller();
        MarshallingInterface inf = null;
        String marshString = marshaller.marshal(inf, MarshallingInterface.class);
        Assert.assertNull(marshString);
    }
}
