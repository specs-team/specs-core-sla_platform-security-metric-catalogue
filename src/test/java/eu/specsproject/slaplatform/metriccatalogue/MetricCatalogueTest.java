package eu.specsproject.slaplatform.metriccatalogue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetric;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricDocument;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;
import eu.specsproject.slaplatform.metriccatalogue.internal.EUMetricManagerSQLJPA;


public class MetricCatalogueTest {

    private static MetricManager metricManager;
    private static SecurityMetric securityMetric;
    private static SecurityMetricIdentifier id;



    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("setUpBeforeClass Called");
        metricManager = MetricManagerFactory.getMetricManagerInstance("test_unit", true);
        Assert.assertNotNull(metricManager);
        System.out.println("metric manager created");
        String  description = readFile("src/test/resources/AMD_timeliness.xml");
        id = metricManager.createSMT(new SecurityMetricDocument("{\"referenceId\" : \"AMD_timeliness.xml\",\"XMLdescription\" : \""+description+"\" ,\"metricType\": \"abstract\"}"));
        System.out.println("metric id returned: "+id.getId());
        Assert.assertNotNull(id);
        Assert.assertNotEquals(0, id.getId()); 
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("tearDownAfterClass Called");
        Assert.assertNotNull(id);
        if ( id != null) 
            metricManager.removeSMT(id);
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
    }

    /*   @After
    public void tearDown() throws Exception {
        System.out.println("tearDown Called");
    }
     */
    @Test
    public final void getMetricManagerIstanceTest() {
        System.out.println("getMetricManagerIstanceTest Called");

        Assert.assertNotNull(metricManager);

    }

    @Test
    public final void getMetricManagerIstanceTestWithParameters(){
        MetricManager m1 = MetricManagerFactory.getMetricManagerInstance("default_unit", true);  
        Assert.assertNotNull(m1);

        MetricManager mm = MetricManagerFactory.getMetricManagerInstance("my_case_unit");  
        Assert.assertNotNull(mm);
    }


    @Test
    public final void EUMetricManagerSQLJPATest(){
        System.out.println("EUMetricManagerSQLJPATest Called");

        EUMetricManagerSQLJPA manager = new EUMetricManagerSQLJPA ( Persistence.createEntityManagerFactory("default_unit"));
        Assert.assertNotNull(manager);
    }

    @Test
    public final void createSMTtest() throws IOException, Exception {
        System.out.println("createSMTtest Called");

        Assert.assertNotNull(metricManager);
        //       String  description = readFile("src/test/resources/AMD_local.xml");
        String  description = readFile("src/test/resources/AMD_CertStatusRequest-2.xml");

        SecurityMetricIdentifier id1 = metricManager.createSMT(new SecurityMetricDocument("{\"referenceId\" : \"AMD_CertStatusRequest-2.xml\",\"XMLdescription\" : \""+description+"\" ,\"metricType\": \"abstract\"}"));
        Assert.assertNotNull(id1);
        Assert.assertNotEquals(0, id1.getId());
        if ( id1 != null) metricManager.removeSMT(id1);
        try {
            id1 = metricManager.createSMT(null);
        }
        catch (Exception e) {
            Assert.assertTrue(true);
        }
    }


    @Test
    public final void retrieveSMTtest() {
        System.out.println("retrieveSMTtest Called");

        Assert.assertNotNull(metricManager.retrieveSMT(id));
        try {
            metricManager.retrieveSMT(null);
        }
        catch (Exception e){
            Assert.assertTrue(true);
        }
        try {
            metricManager.retrieveSMT(new SecurityMetricIdentifier());
        }
        catch (Exception e){
            Assert.assertTrue(true);
        }
    }


    @Test
    public final void removeSMTtest() throws IOException, Exception {
        System.out.println("removeSMTtest Called");

        Assert.assertNotNull(metricManager);
        //          String  description = readFile("src/test/resources/AMD_local.xml");
        String  description = readFile("src/test/resources/AMD_CertStatusRequest-2.xml");

        SecurityMetricIdentifier id1 = metricManager.createSMT(new SecurityMetricDocument("{\"referenceId\" : \"AMD_CertStatusRequest-2.xml\",\"XMLdescription\" : \""+description+"\" ,\"metricType\": \"abstract\"}"));
        Assert.assertNotNull(id1);
        Assert.assertNotEquals(0, id1.getId());
        if ( id1 != null)  id1 = metricManager.removeSMT(id1);
        Assert.assertNotNull(id1);

        try {
            metricManager.removeSMT(null);
        }
        catch ( Exception e){
            Assert.assertTrue(true);
        }
    }


    @Test
    public final void updateSMTtest() throws IOException {
        System.out.println("updateSMTtest Called");

        Assert.assertNotNull(metricManager);
        String  description = readFile("src/test/resources/AMD_CertStatusRequest-2.xml");
        Assert.assertNotNull(metricManager.updateSMT(id, new SecurityMetricDocument("{\"referenceId\" : \"AMD_CertStatusRequest-2.xml\",\"XMLdescription\" : \""+description+"\" ,\"metricType\": \"abstract\"}") ));
        try {
            metricManager.updateSMT(id,null);
        }
        catch(Exception e){
            Assert.assertTrue(true);
        }
    }



    @Test
    public final void annotateSMTtest() {
        System.out.println("annotateSMTtest Called");

        Assert.assertNotNull(metricManager);
        metricManager.annotateSMT(id, "myAnnotationTest"); 

        Assert.assertTrue(true);
    }
    @Test
    public final void getAnnotationsSMTtest() {
        System.out.println("getAnnotationsSMTtest Called");

        Assert.assertNotNull(metricManager);
        metricManager.getAnnotationsSMT(id);
        Assert.assertTrue(true);
    }
    @Test
    public final void updateAnnotationsSMTtest() {
        System.out.println("updateAnnotationsSMTtest Called");

        Assert.assertNotNull(metricManager);
        metricManager.updateAnnotationSMT(id, "annotation", "myNewAnnotation");
        Assert.assertTrue(true);
    }

    @Test
    public final void searchSMTtest() {
        System.out.println("searchSMTtest Called");

        List <SecurityMetricIdentifier> listID = metricManager.searchSMTs("abstract");
        Assert.assertNotNull(listID);
        listID = metricManager.searchSMTs("");
        Assert.assertNotNull(listID);
        listID = metricManager.searchSMTs(null);
        Assert.assertNotNull(listID);
    }


    @Test
    public final void getMetricsBackupTest() {
        System.out.println("getMetricsBackupTest Called");

        File backup = metricManager.getMetricsBackup("jdbc:sqlite:/testpath/db/metric_catalogue.db");

        Assert.assertNull(backup);
    }

    @Test
    public final void restoreMetricsBackupTest() throws FileNotFoundException {
        System.out.println("restoreMetricsBackupTest Called");
        boolean b =    metricManager.restoreMetricsBackup(null, "jdbc:sqlite:/testpath/db/metric_catalogue.db");
        Assert.assertFalse(b);
    }


    private static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString().replace("\"","\\\"");
        } finally {
            br.close();
        }
    }


}
