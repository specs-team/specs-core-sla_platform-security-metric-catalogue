package eu.specsproject.slaplatform.metriccatalogue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specsproject.slaplatform.metriccatalogue.entities.CollectionSchema;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetric;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;

public class MetricCatalogueInternalTest {
    private static SecurityMetric securityMetric;
    private static SecurityMetricIdentifier secIdentifier;
    private static String securityMetricIdentifierId;
    private static CollectionSchema collectionSchema;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		  System.out.println("setUpBeforeClass Called");
		  securityMetric = new SecurityMetric();
	      Assert.assertNotNull(securityMetric);
	      securityMetricIdentifierId = "myID";
	      secIdentifier = new SecurityMetricIdentifier(securityMetricIdentifierId);
	      Assert.assertNotNull(secIdentifier);
	      List<String> lista = new ArrayList<String>();
	      lista.add("myTestList");
	      collectionSchema = new CollectionSchema("myResource",10,5,lista);
	      Assert.assertNotNull(collectionSchema);
		
	}

    @Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
    }
	
	@Test
	public final void securityMetricTest(){
	    SecurityMetric securityMetric1 = new SecurityMetric();
	    Assert.assertNotNull(securityMetric1);		
	}
	
	
	// SecurityMetric TEST
	
	@Test 
	public final void getCreatedTest(){
		Assert.assertNotNull(securityMetric.getCreated());
	}
	
	@Test 
	public final void getUpdatedTest(){
		Assert.assertNotNull(securityMetric.getUpdated());
	}
	
	@Test
	public final void getReferenceIdTest(){
		 // Assert.assertNotNull(securityMetric.getReferenceId());     //Fallisce
		Assert.assertTrue(true);
	}
	
	@Test
	public final void getXMLDescriptionTest(){
		Assert.assertNotNull(securityMetric.getXMLdescription());
	}
	
	@Test
	public final void getMetricTypeTest(){
		Assert.assertNotNull(securityMetric.getMetricType());
	}
	
	
	@Test
	public final void setReferenceIdTest(){
		String id = "myID";
		securityMetric.setReferenceId(id);
		Assert.assertEquals(id, securityMetric.getReferenceId());
	}

	@Test
	public final void setCreatedTest(){
		Date data = new Date();
		securityMetric.setCreated(data);
		Assert.assertEquals(data, securityMetric.getCreated());
	}
	
	
	@Test
	public final void setUpdatedTest(){
		Date data = new Date();
		securityMetric.setUpdated(data);
		Assert.assertEquals(data, securityMetric.getUpdated());
	}
	
	@Test 
	public final void setXMLDescriptionTest(){
		String xmlDescr = "XMLDESCR" ;
		securityMetric.setXMLdescription(xmlDescr);
		Assert.assertEquals(xmlDescr, securityMetric.getXMLdescription());
	}
	
	
	
	@Test
	public final void setMetricTypeTest(){
		String metricType = "myMetricType";
		securityMetric.setMetricType(metricType);
		Assert.assertEquals(metricType, securityMetric.getMetricType());
	}
	
	// SecurityMetricIdentifier Tests
	
	@Test
	public final void securityMetricIdentifierTest(){
		Assert.assertNotNull(secIdentifier);	
	}
	
	@Test
	public final void securityMetricIdentifierNoArgsTest(){
		SecurityMetricIdentifier sec = new SecurityMetricIdentifier();
		Assert.assertNotNull(sec);
	}
	
	@Test
	public final void getIdTest(){
		Assert.assertNotNull(secIdentifier.getId());
	}
	
	@Test
	public final void toStringTest(){
		Assert.assertNotNull(secIdentifier.toString());
	}
	
	@Test
	public final void setIdTest(){
		String myId = "myNewId";
		secIdentifier.setId(myId);
		Assert.assertEquals(myId, secIdentifier.getId());
	}
	
	//CollectionSChema Test
	
	@Test
	public final void collectionSchemaTest(){
		Assert.assertNotNull(collectionSchema);
	}
	
	@Test
	public final void collectionSchemaNoParamsTest(){
		CollectionSchema cs = new CollectionSchema();
		Assert.assertNotNull(cs);
	}
	
	@Test
	public final void getResourceTest(){
		Assert.assertNotNull(collectionSchema.getResource());
		
	}
	
	@Test
	public final void getTotalTest(){
		Assert.assertNotNull(collectionSchema.getTotal());
		
	}
	
	@Test
	public final void getMembersTest(){
		Assert.assertNotNull(collectionSchema.getMembers());
		
	}
	
	@Test
	public final void getItemListTest(){
		Assert.assertNotNull(collectionSchema.getItemList());
		
	}
	
	@Test
	public final void setResourceTest(){
		String res = "myNewResource";
		collectionSchema.setResource(res);
		Assert.assertEquals(res, collectionSchema.getResource());
	}
	
	@Test
	public final void setTotalTest(){
		int total = 9;
		collectionSchema.setTotal(total);
		Assert.assertEquals(Integer.toString(total), Integer.toString(collectionSchema.getTotal()));
	}
	
	@Test
	public final void setMembersTest(){
		int members = 3;
		collectionSchema.setMembers(members);
		Assert.assertEquals(Integer.toString(members), Integer.toString(collectionSchema.getMembers()));
	}
	
	@Test
	public final void setItemListTest(){
		String str1,str2;
	List<String> newList = new ArrayList<String>();
	str1 = "myNewTest";
	newList.add(str1);
	collectionSchema.setItemList(newList);
	newList = collectionSchema.getItemList();
	str2 = newList.get(0);
	Assert.assertEquals(str1, str2);
	
	}
	
	
	//PersistenceManager TEST	
	@Test
	public final void getAttributeFromPersistenceTest(){		
	//	Assert.assertNotNull(PersistenceManager.getAttributeFromPersistence("", ""));
		Assert.assertTrue(true);
	}
	@Test
	public final void getSQLpathByUnitTest(){
		// Assert.assertNotNull(PersistenceManager.getSqlPathByUnit(""));
		Assert.assertTrue(true);
	}
	@Test
	public final void getSQLpathByDbNameTest(){
		 // Assert.assertNotNull(PersistenceManager.getSqlPathByDbName(""));
		Assert.assertTrue(true);
	}
	
	
}
