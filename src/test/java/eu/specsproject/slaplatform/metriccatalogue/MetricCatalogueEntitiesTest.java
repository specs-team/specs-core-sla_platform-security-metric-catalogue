package eu.specsproject.slaplatform.metriccatalogue;


import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import eu.specsproject.slaplatform.metriccatalogue.entities.Annotation;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricDocument;


public class MetricCatalogueEntitiesTest {
	
	private static String id;
	private static String descr;
	private static String securityMetricJsonDocument;
	private static Annotation annotation;
	private static SecurityMetricDocument sec;
	
	@BeforeClass
	public static void setUpBeforeClass(){
		System.out.println("setUpBeforeClass Called");
		id = "id";
		descr = "descr";
		securityMetricJsonDocument = "testing";
		annotation = new Annotation(id,descr);
		Assert.assertNotNull(annotation);
		System.out.println("the annotation id is: "+ annotation.getId());
		Assert.assertNotNull(annotation.getId());
		sec = new SecurityMetricDocument(securityMetricJsonDocument);
		Assert.assertNotNull(sec);
		
		
	}
	
	@Before
    public void setUp() throws Exception {
        System.out.println("setUp Called");
    }

	@Test
	public final void annotationTest() {
		Annotation a = new Annotation (id,descr);
		Assert.assertNotNull(a);
	
	}
	@Test
	public final void annotationTestNoParameters() {
		Annotation a = new Annotation();
		Assert.assertNotNull(a);
	}
    @Test
    public final void getIdTest(){
    	Assert.assertNotNull(annotation);
    	Assert.assertNotNull(annotation.getId());
    }
    
    @Test
    public final void getDescrTest(){
    	Assert.assertNotNull(annotation);
    	Assert.assertNotNull(annotation.getDescr());
    }
    
    @Test
    public final void setIdTest(){
    	Assert.assertNotNull(annotation);
    	annotation.setId("test");
       Assert.assertEquals("test", annotation.getId());
    }
    
    @Test
    public final void setDescrTest(){
    	Assert.assertNotNull(annotation);
    	annotation.setDescr("description test");
    	Assert.assertEquals("description test", annotation.getDescr());
    	
    }
    
    @Test
    public final void securityMetricDocumentTest(){
    	Assert.assertNotNull(sec);
    }
    
    @Test
    public final void securityMetricDocumentTestNoParams(){
    	SecurityMetricDocument sec2 = new SecurityMetricDocument();
    	Assert.assertNotNull(sec2);
    }
    
    @Test
    public final void getSecurityMetricJsonDocumentTest(){
    	Assert.assertNotNull(sec);
    	String document = sec.getSecurityMetricJsonDocument();
    	Assert.assertNotNull(document);
    	Assert.assertNotEquals("",document);
    }
   
    @Test
    public final void  setSecurityMetricJsonDocumentTest(){
    	Assert.assertNotNull(sec);
    	String document = "test2";
    	sec.setSecurityMetricJsonDocument(document);    	
    	Assert.assertEquals(document,sec.getSecurityMetricJsonDocument());
    }
}
